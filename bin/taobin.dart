import 'dart:io';

void main() {
  print("\nStart og to Taobin");

  List<Map<String, int>> product = [
    {'green tea': 50},
    {'hot tea': 40},
    {'cocoa': 50},
    {'chocolate protein shake': 50},
    {'iced plum soda': 40},
    {'oreo Chocy': 50},
    {'green tea': 50},
    {'hot black tea': 40},
    {'iced plum soda': 50},
  ];

  List<Map<String, int>> it;

  List<String> sweet = [
    'No gugar 0%',
    'sweet 25%',
    'sweet 50%',
    'sweet 75%',
    'sweet 100%',
    'sweet 120%',
  ];

  int? list, level, ca;
  List<String> cart = [];
  int count = 0;

  print("\nรายการเมนู");
  print("\n");
  while (true) {
    for (int i = 0; i < product.length; i++) {
      print(
          "เมนูที่ ${i + 1}. ${product[i].keys.first} ราคา ${product[i].values.first} บาท");
    }

    print("\nเลือกเมนูที่ต้องการ");
    while (true) {
      list = int.parse(stdin.readLineSync()!);
      if (list < 1 || list > product.length) {
        print("ไม่มีเมนู กรุณาใส่เลขใหม่อีกครั้ง");
      } else {
        break;
      }
    }
    print("\nรายการเมนู");
    for (int i = 0; i < sweet.length; i++) {
      print("ระดับความหวาน ${i + 1}. ${sweet[i]}");
    }
    print("\nเลือกระดับความหวานที่ต้องการ");
    while (true) {
      level = int.parse(stdin.readLineSync()!);
      if (level < 1 || level > sweet.length) {
        print("ไม่มีระดับความหวานระดับนี้ กรุณาใส่เลขใหม่");
      } else {
        break;
      }
    }
    print("\nต้องการเพิ่มสินค้าอีกรึไม่ (กรุณาพิมพ์ YES หรือ NO เท่านั้น)");
    String? addproduct;
    while (true) {
      addproduct = stdin.readLineSync();
      cart.add('${list - 1},${level - 1},$ca');
      if (addproduct == 'NO' || addproduct == 'YES') {
        break;
      }
    }
    if (addproduct == 'NO') {
      break;
    }
  }

  print('\nรายการของคุณ ได้แก่');
  for (int i = 0; i < cart.length; i++) {
    List<String> particular = cart[i].split(',');

    it = product;

    count += it[int.parse(particular[0])].values.first;
    print(
        ' ${i + 1}. ${it[int.parse(particular[0])].keys.first} ระดับความหวานที่ต้องการ ${sweet[int.parse(particular[1])]} ราคา ${it[int.parse(particular[0])].values.first} บาท');
  }
  print('รวมทั้งหมด $count บาท\n');
}
